# Mapzilla

Mapzilla is an android app that allows users to see restaurants near them using AR, along with their ratings, price range and opening hours; In case the user wants to check a location where he is not present currently, he can search for any location and get the same information using a 2D map.

## Getting Started:

Follow the directions here to generate your API key: https://developers.google.com/maps/documentation/android/start#get-key .
The key will need to have the Google Places API active, otherwise it will not work.


# Instructions


The app has two separate activities one the app:

## AR Activity

This is used to check restaurants near the user. Through AR, they will be able to see restaurants on the selected range, and the corresponding information to them.

## 2D Map Activity

This is used to simulate a location and check the availability of restaurants near a picked location, and also the corresponding information.


For more detailed instructions on how to use the app, check the information activity


# Libraries and APIs

* Google Maps 
* Google Places API
* Google Details API
* ARCore
* Retrofit

# Bugs

There are several known bugs, will list here the most notorious ones:

* AR Directions not always work properly
* App crashes when selecting marker on AR with that has no info
* On 2D view, the opening hours are not working properly
* AR markers placing behaving weirdly sometimes

# TO DO

Besides fixing the known bugs, there are several features that have not been implemented yet, such as:

* Responsive design UI
* Filtering restaurants by ratings and opening hours
* Favorites restaurants by user
* Try to display the restaurant menu, or at least the food type offered

