package com.example.mapzilla

import com.google.gson.annotations.SerializedName
/**
 * Data class encapsulating a response from the detail search call to the Details API.
 */
data class DetailPlacesResponse(
    @SerializedName("result") val result: PlaceDetail
)
