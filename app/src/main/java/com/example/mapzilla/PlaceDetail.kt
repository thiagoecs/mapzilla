package com.example.mapzilla
/**
 * A model describing details about a PlaceDetail (id, opening_hours, weekday_text).
 */
data class PlaceDetail(
    val id: String,
    val opening_hours: PlaceOpeningHours
) {
    override fun equals(other: Any?): Boolean {
        if (other !is PlaceDetail) {
            return false
        }
        return this.id == other.id
    }

    override fun hashCode(): Int {
        return this.id.hashCode()
    }

    data class PlaceOpeningHours (
        val weekday_text: Array<String>
        )
}
