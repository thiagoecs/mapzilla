package com.example.mapzilla

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.TextView
import com.google.ar.sceneform.Node
import com.google.ar.sceneform.rendering.ViewRenderable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

//Class for building the AR marker after clicking specific restaurant on main view.
class PlaceNode(
    private val context: Context,
    val place: Place?
) : Node() {

    private lateinit var placesServices: PlacesService

    private var placeRenderable: ViewRenderable? = null
    var textViewPlace: TextView? = null
    private var textViewRating: TextView? = null
    private var textViewPrice: TextView? = null
    private var textViewOpeningHoursText: TextView? = null
    private var textViewOpeningHours: TextView? = null
    private var places: PlaceDetail? = null

    override fun onActivate() {
        super.onActivate()

        placesServices = PlacesService.create()

        if (scene == null) {
            return
        }

        if (placeRenderable != null) {
            return
        }

        //Builds the AR marker
        ViewRenderable.builder()
            .setView(context, R.layout.place_view)
            .build()
            .thenAccept { renderable ->
                setRenderable(renderable)
                placeRenderable = renderable

                place?.let {

                    //Gets the specific place_id of the restaurant and sends it to the
                    //getDetailPlaces function. After that another API call runs and we get opening_hours.
                    val placeId = it.place_id
                    getDetailPlaces(placeId)

                    //Finding the right textViews
                    textViewPlace = renderable.view.findViewById(R.id.placeName)
                    textViewPlace?.text = it.name
                    textViewOpeningHoursText = renderable.view.findViewById(R.id.openingHoursText)

                    val priceInt = it.price_level.toInt()
                    val rating = it.rating
                    val ratingAmount = it.user_ratings_total + " Reviews"

                    textViewPlace?.setOnClickListener {
                        textViewPrice = renderable.view.findViewById(R.id.price)
                        textViewRating = renderable.view.findViewById(R.id.ratings)

                        textViewRating?.text = context.getString(R.string.rating,rating,ratingAmount)
                        textViewOpeningHoursText?.text = context.getString(R.string.opening_hours)

                        //API gives price level as a number so this "changes" them to text
                        if (priceInt == 1) {
                            textViewPrice?.text = context.getString(R.string.inexpensive)
                        }else if (priceInt == 2){
                            textViewPrice?.text = context.getString(R.string.moderate)
                        }else if (priceInt == 3){
                            textViewPrice?.text = context.getString(R.string.expensive)
                        }else if (priceInt == 4){
                            textViewPrice?.text = context.getString(R.string.very_expensive)
                        }else {
                            textViewPrice?.text = context.getString(R.string.no_price)
                        }

                        //Makes textViews visible after clicking restaurant in AR
                        textViewOpeningHoursText.let {
                            it?.visibility =
                                if (it?.visibility == View.VISIBLE) View.GONE else View.VISIBLE
                        }

                        textViewPrice?.let {
                            it.visibility =
                                if (it.visibility == View.VISIBLE) View.GONE else View.VISIBLE
                        }

                        textViewRating?.let {
                            it.visibility = if (it.visibility == View.VISIBLE) View.GONE else View.VISIBLE
                        }
                    }

                    textViewOpeningHoursText?.setOnClickListener {
                        textViewOpeningHours = renderable.view.findViewById(R.id.opening_hours)

                        //Adds all of the opening hours to the textView.
                        for (i in 0..(places?.opening_hours?.weekday_text?.size?.minus(1)!!)) {
                            textViewOpeningHours.let {
                                it?.append(places?.opening_hours?.weekday_text?.get(i) + "\n      ")
                            }
                        }

                        textViewOpeningHours.let {
                            it?.visibility =
                                if (it?.visibility == View.VISIBLE) View.GONE else View.VISIBLE
                        }
                    }

                    //Checks how big of a rating specific restaurant has and sets the correct image
                    //for the AR marker.
                    val ratingInt = it.rating.toDouble()
                    if (ratingInt >= 3.5 && ratingInt <= 4.5) {
                        textViewPlace?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.stars_4, 0,0,0)
                    }else if (ratingInt > 4.5) {
                        textViewPlace?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.stars_5, 0,0,0)
                    }else if (ratingInt < 3.5 && ratingInt >= 2.5) {
                        textViewPlace?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.stars_3, 0,0,0)
                    }else if (ratingInt < 2.5 && ratingInt >= 1.5) {
                        textViewPlace?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.stars_2, 0,0,0)
                    }else if (ratingInt < 1.5 && ratingInt >= 0.5) {
                        textViewPlace?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.stars_1, 0,0,0)
                    }else {
                        textViewPlace?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.question_mark, 0, 0, 0)
                    }
                }
            }
    }

    fun showInfoWindow() {
        // Show text
        textViewPlace?.let {
            it.visibility = if (it.visibility == View.VISIBLE) View.GONE else View.VISIBLE
        }

        // Hide text for other nodes
        this.parent?.children?.filter {
            it is PlaceNode && it != this
        }?.forEach {
            (it as PlaceNode).textViewPlace?.visibility = View.GONE
        }
    }

    //API call to get opening_hours for the specific restaurant
    private fun getDetailPlaces(placeId: String) {
        val apiKey = this.context.getString(R.string.google_maps_key)
        placesServices.detailPlaces(
            apiKey = apiKey,
            placeId = placeId,
            fields = "name,rating,opening_hours/weekday_text"
        ).enqueue(
            object : Callback<DetailPlacesResponse> {
                override fun onFailure(call: Call<DetailPlacesResponse>, t: Throwable) {
                    Log.e("Detail API", "Failed to get place details", t)
                }

                override fun onResponse(
                    call: Call<DetailPlacesResponse>,
                    response: Response<DetailPlacesResponse>
                ) {
                    if (!response.isSuccessful) {
                        Log.e("Detail API", "Failed to get place details")
                        return
                    }
                    Log.d("Response", response.toString())
                    val places = response.body()?.result
                    this@PlaceNode.places = places
                }
            }
        )
    }
}