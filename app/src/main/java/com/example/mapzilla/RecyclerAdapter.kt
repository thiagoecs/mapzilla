package com.example.mapzilla

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter: RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    private var titles = arrayOf("Using the AR feature: 1", "Using the AR feature: 2","Using the AR feature: 3", "Using the AR feature: 4",
    "Using the location feature: 1","Using the location feature: 2","Using the location feature: 3","Using the location feature: 4","Using the location feature: 5")

    private var details = arrayOf("Look around in the AR until you see some grey dots on the floor. Click them!",
        "Now you should see markers of nearby restaurants in the map below. Click one of the restaurants.",
        "Now look around in the AR mode again and you will find the restaurant that you clicked in there.",
        "Click the restaurant to see more information.", "Click the 2D button on top left corner.","Search for a location using the search bar.",
        "Click the button on top right corner to see nearby restaurants.","You can change the radius of the restaurants shown","Restaurants searched with maximum radius")

    private var images = intArrayOf(R.drawable.picture1mapzilla, R.drawable.picture2mapzilla, R.drawable.picture3mapzilla, R.drawable.picture4mapzilla,
        R.drawable.mapzillakuvajuttu1,R.drawable.mapzillapictures1, R.drawable.mapzillapictures2, R.drawable.mapzillapictures3, R.drawable.mapzillapictures4)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerAdapter.ViewHolder {
       val v = LayoutInflater.from(parent.context).inflate(R.layout.card_layout, parent, false)
        return ViewHolder(v)
    }
    override fun getItemCount(): Int {
        return titles.size
    }
    override fun onBindViewHolder(holder: RecyclerAdapter.ViewHolder, position: Int) {
        holder.itemTitle.text = titles[position]
        holder.itemDetail.text = details[position]
        holder.itemImage.setImageResource(images[position])
    }


    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var itemImage: ImageView
        var itemTitle: TextView
        var itemDetail: TextView

        init {
            itemImage = itemView.findViewById(R.id.item_image)
            itemTitle = itemView.findViewById(R.id.item_title)
            itemDetail = itemView.findViewById(R.id.item_detail)
        }

    }

}