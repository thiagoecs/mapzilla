package com.example.mapzilla

import android.Manifest
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.core.content.getSystemService
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_second_map.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import android.widget.TextView

import android.graphics.Typeface

import android.view.Gravity
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter


class SecondMap : AppCompatActivity(), SensorEventListener {

    private val TAG = "SecondMap"

    private lateinit var placesService: PlacesService
    private lateinit var mapFragment: SupportMapFragment

    // Location
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    // Sensor
    private lateinit var sensorManager: SensorManager
    private val accelerometerReading = FloatArray(3)
    private val magnetometerReading = FloatArray(3)
    private val rotationMatrix = FloatArray(9)
    private val orientationAngles = FloatArray(3)

    private var markers: MutableList<Marker> = emptyList<Marker>().toMutableList()
    private var places: List<Place>? = null
    private var placesDetail: PlaceDetail? = null
    private var currentLocation: Location? = null
    private var map: GoogleMap? = null
    // creating a variable
    // for search view.
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!isSupportedDevice()) {
            return
        }
        setContentView(R.layout.activity_second_map)

        mapFragment =
            supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment

        sensorManager = getSystemService()!!
        placesService = PlacesService.create()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

            // initializing our search view.
            val searchView = findViewById<SearchView>(R.id.idSearchView)
            searchView.queryHint = "Search Here"
            // Obtain the SupportMapFragment and get notified
            // when the map is ready to be used.
            // adding on query listener for our search view.
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    map?.clear()
                    // on below line we are getting the
                    // location name from search view.
                    val location = searchView.query.toString()

                    // below line is to create a list of address
                    // where we will store the list of all address.
                    var addressList: List<Address>? = null

                    // checking if the entered location is null or not.
                    // on below line we are creating and initializing a geo coder.
                    val geocoder = Geocoder(this@SecondMap)
                    try {
                        // on below line we are getting location from the
                        // location name and adding that location to address list.
                        addressList = geocoder.getFromLocationName(location, 1)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    if (addressList != null && addressList.isNotEmpty()) {
                        // on below line we are getting the location
                        // from our list a first position.
                        val address = addressList[0]

                        // on below line we are creating a variable for our location
                        // where we will add our locations latitude and longitude.
                        val latLng = LatLng(address.latitude, address.longitude)

                        val distance = (Location(LocationManager.GPS_PROVIDER).apply {
                            latitude = address.latitude
                            longitude = address.longitude
                        })
                        getNearbyPlaces(distance)


                        // on below line we are adding marker to that position.
                        map!!.addMarker(MarkerOptions().position(latLng).title(location))

                        // below line is to animate camera to that position.
                        map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
                        searchView.queryHint = "Search Here"
                    }else{
                        searchView.setQuery("", false)
                        searchView.queryHint = "Try Again"
                    }
                    return false
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    return false
                }
            })


        setUpMaps()

        //Range slider for the 2D map so user can get restaurants closer to the location
        //or further if he wants.
        seekBar2.max = 1000
        seekBar2.min = 0
        seekBar2.progress = 500

        seekBar2.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(
                seek: SeekBar,
                progress: Int, fromUser: Boolean
            ) {
                // Could write code for when progress is changed
            }

            override fun onStartTrackingTouch(seek: SeekBar) {
                // Could write code for when progress is touched
            }

            override fun onStopTrackingTouch(seek: SeekBar) {
                // Shows the valued of range slider as a toast after letting go
                Toast.makeText(
                    this@SecondMap,
                    "Range is: " + seek.progress + "m",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

        val button = findViewById<Button>(R.id.changeMap)
        button.setOnClickListener {
            addPlaces()
        }

        val chgMap = findViewById<FloatingActionButton>(R.id.changeMaps)
        chgMap.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }

    private fun addPlaces() {
        val currentLocation = currentLocation
        if (currentLocation == null) {
            Log.w(TAG, "Location has not been determined yet")
            return
        }

        val places = places
        if (places == null) {
            Log.w(TAG, "No places to put")
            return
        }
        for (place in places) {
            //Sends the place_id's for getDetailPlaces function
            getDetailPlaces(place.place_id)
            map?.let {

                //Adds markers on the map and puts same info on them as on the AR marker.
                //Opening_hours would still need some work though.
                val marker = it.addMarker(
                    MarkerOptions()
                        .position(place.geometry.location.latLng)
                        .title(place.name)
                        .snippet("Rating: "+place.rating+"\n"+"Reviews: "+
                                place.user_ratings_total+"\n"+
                                "Price level: "+place.price_level+
                                "\n"+placesDetail?.opening_hours?.weekday_text?.get(0)+"\n"+placesDetail?.opening_hours?.weekday_text?.get(1)+"\n"+placesDetail?.opening_hours?.weekday_text?.get(2)
                                +"\n"+placesDetail?.opening_hours?.weekday_text?.get(3)+"\n"+placesDetail?.opening_hours?.weekday_text?.get(4)+"\n"+placesDetail?.opening_hours?.weekday_text?.get(5)
                                +"\n"+placesDetail?.opening_hours?.weekday_text?.get(6))
                )

                map!!.setInfoWindowAdapter(object : InfoWindowAdapter {
                    override fun getInfoWindow(arg0: Marker): View? {
                        return null
                    }

                    override fun getInfoContents(marker: Marker): View? {
                        val info = LinearLayout(this@SecondMap)
                        info.orientation = LinearLayout.VERTICAL
                        val title = TextView(this@SecondMap)
                        title.setTextColor(Color.BLACK)
                        title.gravity = Gravity.CENTER
                        title.setTypeface(null, Typeface.BOLD)
                        title.text = marker.title
                        val snippet = TextView(this@SecondMap)
                        snippet.setTextColor(Color.GRAY)
                        snippet.text = marker.snippet
                        info.addView(title)
                        info.addView(snippet)
                        return info
                    }
                })
                showInfoWindow(place)
                if (marker != null) {
                    marker.tag = place
                }
                if (marker != null) {
                    markers.add(marker)
                }
            }
        }
    }

    private fun showInfoWindow(place: Place) {

        // Show as marker
        val matchingMarker = markers.firstOrNull {
            val placeTag = (it.tag as? Place) ?: return@firstOrNull false
            return@firstOrNull placeTag == place
        }
        matchingMarker?.showInfoWindow()
    }


    private fun setUpMaps() {
        mapFragment.getMapAsync { googleMap ->
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return@getMapAsync
            }
            googleMap.isMyLocationEnabled = true

            getCurrentLocation {
                val pos = CameraPosition.fromLatLngZoom(it.latLng, 13f)
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(pos))
            }
            map = googleMap
        }
    }

    private fun getCurrentLocation(onSuccess: (Location) -> Unit) {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationClient.lastLocation.addOnSuccessListener { location ->
            currentLocation = location
            onSuccess(location)
        }.addOnFailureListener {
            Log.e(TAG, "Could not get location")
        }
    }

    //API call to get nearby places in the location user wrote
    private fun getNearbyPlaces(location: Location) {
        val apiKey = this.getString(R.string.google_maps_key)
        placesService.nearbyPlaces(
            apiKey = apiKey,
            location = "${location.latitude},${location.longitude}",
            radiusInMeters = seekBar2.progress,
            placeType = "restaurant"
        ).enqueue(
            object : Callback<NearbyPlacesResponse> {
                override fun onFailure(call: Call<NearbyPlacesResponse>, t: Throwable) {
                    Log.e(TAG, "Failed to get nearby places", t)
                }

                override fun onResponse(
                    call: Call<NearbyPlacesResponse>,
                    response: Response<NearbyPlacesResponse>
                ) {
                    if (!response.isSuccessful) {
                        Log.e(TAG, "Failed to get nearby places")
                        return
                    }

                    val places = response.body()?.results ?: emptyList()
                    this@SecondMap.places = places
                }
            }
        )
    }

    //API call to get opening_hours for the restaurants.
    private fun getDetailPlaces(placeId: String) {
        val apiKey = this.getString(R.string.google_maps_key)
        placesService.detailPlaces(
            apiKey = apiKey,
            placeId = placeId,
            fields = "name,rating,opening_hours/weekday_text"
        ).enqueue(
            object : Callback<DetailPlacesResponse> {
                override fun onFailure(call: Call<DetailPlacesResponse>, t: Throwable) {
                    Log.e("Detail API", "Failed to get place details", t)
                }

                override fun onResponse(
                    call: Call<DetailPlacesResponse>,
                    response: Response<DetailPlacesResponse>
                ) {
                    if (!response.isSuccessful) {
                        Log.e("Detail API", "Failed to get place details")
                        return
                    }
                    Log.d("Response", response.toString())
                    val places = response.body()?.result
                    this@SecondMap.placesDetail = places
                    Log.d("Weekday", places.toString())
                }
            }
        )
    }

    //Checks if the device supports AR
    private fun isSupportedDevice(): Boolean {
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val openGlVersionString = activityManager.deviceConfigurationInfo.glEsVersion
        if (openGlVersionString.toDouble() < 3.0) {
            Toast.makeText(this, "SceneForm requires OpenGL ES 3.0 or later", Toast.LENGTH_LONG)
                .show()
            finish()
            return false
        }
        return true
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (event == null) {
            return
        }
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            System.arraycopy(event.values, 0, accelerometerReading, 0, accelerometerReading.size)
        } else if (event.sensor.type == Sensor.TYPE_MAGNETIC_FIELD) {
            System.arraycopy(event.values, 0, magnetometerReading, 0, magnetometerReading.size)
        }

        // Update rotation matrix, which is needed to update orientation angles.
        SensorManager.getRotationMatrix(
            rotationMatrix,
            null,
            accelerometerReading,
            magnetometerReading
        )
        SensorManager.getOrientation(rotationMatrix, orientationAngles)
    }
}